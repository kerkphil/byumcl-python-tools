=================
byumcl test suite
=================

In this test suite there will be both serial and parallel tests. Currenlty, the
tools that are being used for testing are somewhat incompatible with each
other. In particular, I have had trouble using "nose" in my parallel tests. The
easiest solution, it seems, it to seperate the two types of tests. The
following is the general protocol for testing:

Serial tests can be run using Unittest. Just run the command `python -m
unittest discover`.

To run the parallel tests, navigate to the mpinoseutils test folder and run
`mpiexec -np 10 python runtests.py -w ../byumcl/test/` (on Windows, use
python.exe).




