"""
Created August 1, 2012

Author: Spencer Lyon

This python script implements routines described in '"Numerically Stable
and Accurate Stochastic Simulation Approaches for Solving Dynamic
Economic Models" by Kenneth L. Judd, Lilia Maliar and Serguei Maliar,
(2011), Quantitative Economics 2/2, 173-210 (henceforth, JMM, 2011).'

As such, it is adapted from the file .Num_Stab_Approx.m'.

These functions all match the JMM MatLab code, but the results from both
files are different than the solution that would be obtained with normal
least squares estimation.
"""
from __future__ import division
from numpy import std, mean, zeros, diag, empty, dot, eye
from scipy.linalg import lstsq, inv, svd
# from cvxopt import solvers

__all__ = ['normalize_data', 'OLS', 'LS_SVD', 'RLS_Tikhonov', 'RLS_TSVD']


def normalize_data(X, Y, intercept=True):
    """
    This function will normalize the data to be used in the regression.
    This typically leads to more stable regression results.
    It is assumed that the regression is set up with X as the independent
    variables, Y as the dependent one, and beta as the coefficients in this way:

        Y = beta0 + x1 * beta1 + x * beta2 + ... xn * betan = X * Beta

    Parameters
    ----------
    X: numpy array, dtype=float, shape= (T x n)
        This is the array of data that comes from the independent variables.
        It is assumed that if an intercept term is included in the regression
        that is is represented in this matrix the first column being 1.

    Y: numpy array, dtype=float, shape = (T x NN)
        This is the dependent variable that is to be regressed. If there is
        more than one column it is assumed that multiple simulatneous
        regressions are to be run.

    intercept: bool, optional(default=True)
        Whether or not an intercept term is included in the regression.

    Returns
    -------
    X1: numpy array, dtype=float, shape=(T x n1)
        The normalized independent variables. n1 is the new number of parameters
        If there is an intercept term it is equal to n - 1. Otherwise it is just
        equal to n.

    Y1: numpy array, dtype-float, shape=(T x NN)
        The normalized dependent variable.
    """
    if intercept == True:
        X1 = (X[:, 1:] - mean(X[:, 1:], axis=0)) / std(X[:, 1:], axis=0, ddof=1)
        Y1 = (Y - mean(Y, axis=0)) / std(Y, axis=0, ddof=1)

    else:
        X1 = (X - mean(X, axis=0)) / std(X, axis=0, ddof=1)
        Y1 = (Y - mean(Y, axis=0)) / std(Y, axis=0, ddof=1)

    return X1, Y1


def OLS(X, Y, normalize=True, intercept=True):
    """
    Does ordinary least squares (OLS) regression of Y onto X.

    Parameters
    ----------
    X: numpy array, dtype=float, shape= (T x n)
        This is the array of data that comes from the independent
        variables. It is assumed that if an intercept term is included
        in the regression that is is represented in this matrix the
        first column being 1.

    Y: numpy array, dtype=float, shape = (T x NN)
        This is the dependent variable that is to be regressed. If
        there is more than one column it is assumed that multiple
        simulatneous regressions are to be run.

    normalize: boolean, optional(default=True)
        Bool telling whether or not you want to normalize the data
        before the regression starts.

    intercept: bool, optional(default=True)
        Whether or not an intercept term is included in the regression

    Returns
    -------
    Beta: array, dtype=float, shape=(n x 1)
        The array of coefficients that minimizes the sum of squared
        errors.
    """
    if normalize:
        X1, Y1 = normalize_data(X, Y, intercept)
        Beta_1 = (lstsq(X1, Y1)[0]).squeeze()
        Beta = empty(Beta_1.size + 1)
        Beta[1:] = (1. / std(X[:,1:], axis=0, ddof=1)) * Beta_1 * std(Y, axis=0, ddof=1)
        Beta[0] = mean(Y) - dot(mean(X[:, 1:], axis=0), Beta[1:])

    else:
        if intercept:
            X1, Y1 = [X[:, 1:], Y]
            Beta_1 = (lstsq(X1, Y1)[0]).squeeze()
            Beta = empty(Beta_1.size + 1)
            Beta[1:] = Beta_1
            Beta[0] = mean(Y) - dot(mean(X[:, 1:], axis=0), Beta[1:])
        else:
            X1, Y1, = [X, Y]
            Beta = lstsq(X1, Y1)[0]

    return Beta


def LS_SVD(X, Y, normalize=True, intercept=True):
    """
    Does least squares regression of Y onto X using the singular value
    decomposition of X. In ill-conditioned problems this is a more
    reliable method than standard OLS. In normal problems there is no
    difference.

    Parameters
    ----------
    X: numpy array, dtype=float, shape= (T x n)
        This is the array of data that comes from the independent
        variables. It is assumed that if an intercept term is included
        in the regression that is is represented in this matrix the
        first column being 1.

    Y: numpy array, dtype=float, shape = (T x NN)
        This is the dependent variable that is to be regressed. If
        there is more than one column it is assumed that multiple
        simulatneous regressions are to be run.

    normalize: boolean, optional(default=True)
        Bool telling whether or not you want to normalize the data
        before the regression starts.

    intercept: bool, optional(default=True)
        Whether or not an intercept term is included in the regression

    Returns
    -------
    Beta: array, dtype=float, shape=(n x 1)
        The array of coefficients that minimizes the sum of squared
        errors.
    """
    if normalize:
        X1, Y1 = normalize_data(X, Y, intercept)
        U, s, Vh = svd(X1, full_matrices=False)
        V = Vh.T
        S_inv = diag(1. / s)
        Beta_1 = dot(dot(dot(V, S_inv), U.T), Y1).squeeze()
        Beta = empty(Beta_1.size + 1)
        Beta[1:] = (1. / std(X[:,1:], axis=0, ddof=1)) * std(Y, ddof=1) * Beta_1
        Beta[0] = mean(Y) - dot(mean(X[:,1:], axis=0), Beta[1:])

    else:
        X1, Y1, = [X, Y]
        U, s, Vh = svd(X1, full_matrices=False)
        V = Vh.T
        S_inv = diag(1. / s)
        Beta = dot(dot(dot(V, S_inv), U.T), Y1).squeeze()


    return Beta


def RLS_Tikhonov(X, Y, penalty, normalize=True, intercept=True):
    """
    Does least squares regression of Y onto X using Tikhonov
    regularization. This will replace an ill-conditioned problem
    with a well conditioned one. This happens by imposing an L2
    penalty on the ill-conditioned parts. The solution takes the
    following form:
        :math:`beta = (X.T * X + eta * eye).inv() * X.T * y`

    Where :math:`eta` is the penalty parameter.

    Parameters
    ----------
    X: numpy array, dtype=float, shape= (T x n)
        This is the array of data that comes from the independent
        variables. It is assumed that if an intercept term is included
        in the regression that is is represented in this matrix the
        first column being 1.

    Y: numpy array, dtype=float, shape = (T x NN)
        This is the dependent variable that is to be regressed. If
        there is more than one column it is assumed that multiple
        simulatneous regressions are to be run.

    penalty: number, float
        A parameter determining the value of the regularization
        parameter. NEGATIVE NUMBER

    normalize: boolean, optional(default=True)
        Bool telling whether or not you want to normalize the data
        before the regression starts.

    intercept: bool, optional(default=True)
        Whether or not an intercept term is included in the regression

    Returns
    -------
    Beta: array, dtype=float, shape=(n x 1)
        The array of coefficients that minimizes the sum of squared
        errors.
    """
    T, n = X.shape
    if intercept:
        n1 = n -1
    else:
        n1 = 1

    if normalize:
        X1, Y1 = normalize_data(X, Y, intercept)
        B_1 = dot(dot(inv(dot(X1.T, X1) + T / n1 * eye(n1) * 10 ** penalty),
                      X1.T), Y1).squeeze()
        Beta = empty(B_1.size + 1)
        Beta[1:] = (1. / std(X[:,1:], axis=0, ddof=1)) * std(Y, ddof=1) * B_1
        Beta[0] = mean(Y) - dot(mean(X[:,1:], axis=0), Beta[1:])

    else:
        X1, Y1, = [X, Y]
        Beta = dot(dot(inv(dot(X1.T, X1) + T / n1 * eye(n1) * 10 ** penalty),
                       X1.T), Y1)

    return Beta


def RLS_TSVD(X, Y, penalty, normalize=True, intercept=True):
    """
    Does least squares regression of Y onto X using Tikhonov
    regularization and the singular value decomposition. This will
    replace an ill-conditioned problem with a well conditioned one.
    This happens by imposing an L2 penalty on the ill-conditioned
    parts. The solution takes the following form:
        beta = (X.T * X + eta * eye).inv() * X.T * y

    Where eta is the penalty parameter.

    Parameters
    ----------
    X: numpy array, dtype=float, shape= (T x n)
        This is the array of data that comes from the independent
        variables. It is assumed that if an intercept term is included
        in the regression that is is represented in this matrix the
        first column being 1.

    Y: numpy array, dtype=float, shape = (T x NN)
        This is the dependent variable that is to be regressed. If
        there is more than one column it is assumed that multiple
        simulatneous regressions are to be run.

    penalty: number, float
        A parameter determining the value of the regularization
        parameter. POSITIVE NUMBER

    normalize: boolean, optional(default=True)
        Bool telling whether or not you want to normalize the data
        before the regression starts.

    intercept: bool, optional(default=True)
        Whether or not an intercept term is included in the regression

    Returns
    -------
    Beta: array, dtype=float, shape=(n x 1)
        The array of coefficients that minimizes the sum of squared
        errors.
    """
    T, n = X.shape
    if intercept:
        n1 = n -1
    else:
        n1 = n

    if normalize:
        X1, Y1 = normalize_data(X, Y, intercept)
        U, s, Vh = svd(X1, full_matrices=False)
        V = Vh.T
        r = sum(max(s) / s <= 10 ** penalty)
        Sr_inv = zeros((n1, n1))
        Sr_inv[:r, :r] = diag(1. / s[:r])
        Beta_1 = dot(dot(dot(V, Sr_inv), U.T), Y1).squeeze()
        Beta = empty(Beta_1.size + 1)
        Beta[1:] = (1. / std(X[:,1:], axis=0, ddof=1)) * std(Y, ddof=1) * Beta_1
        Beta[0] = mean(Y) - dot(mean(X[:,1:], axis=0), Beta[1:])

    else:
        X1, Y1, = [X, Y]
        U, s, Vh = svd(X1, full_matrices=False)
        V = Vh.T
        r = sum(max(s) / s <= 10 ** penalty)
        Sr_inv = zeros((n1, n1))
        Sr_inv[:r, :r] = diag(1. / s[:r])
        Beta = dot(dot(dot(V, Sr_inv), U.T), Y1).squeeze()

    return Beta



#def LAD_PP(X, Y, normalize=True, intercept=True):
#    """
#    Does the regression of Y onto X by first converting the problem
#    to a least absolute deviation (LAD) problemm. This particular
#    method then uses the associted linear program and solved the
#    primal problem. The problem can be written as follows:
#          min      (v+) + (v-)
#       v+, v-, b
#
#          s.t.     (x+) =- (v-) + Xb = y
#                   (v+) >= 0 (v-) >= 0
#
#
#    Parameters
#    ----------
#    X: numpy array, dtype=float, shape= (T x n)
#        This is the array of data that comes from the independent
#        variables. It is assumed that if an intercept term is included
#        in the regression that is is represented in this matrix the
#        first column being 1.
#
#    Y: numpy array, dtype=float, shape = (T x NN)
#        This is the dependent variable that is to be regressed. If
#        there is more than one column it is assumed that multiple
#        simulatneous regressions are to be run.
#
#    normalize: boolean, optional(default=True)
#        Bool telling whether or not you want to normalize the data
#        before the regression starts.
#
#    intercept: bool, optional(default=True)
#        Whether or not an intercept term is included in the regression
#
#    Returns
#    -------
#    Beta: array, dtype=float, shape=(n x 1)
#        The array of coefficients that minimizes the abosolute
#        deviations.
#    """
#    T, n = X.shape
#    Y = Y if Y.ndim == 2 else expand_dims(Y, axis=1)
#    N = Y.shape[1]
#
#    if normalize:
#        X1, Y1 = normalize_data(X, Y, intercept)
#
#        n1 = n - 1
#        lower_b = zeros((2 * T + 1, 1))
#        lower_b[:n1] -= 100
#
#        upper_b = ones((2 * T + 1, 1))
#        upper_b[:n1] += 99
#        upper_b[n1:] *= inf
#
#        f = ones((2 * T + 1, 1))
#        f[:n1] = 0.0
#        Aeq = hstack((X1, eye(T), -eye(T)))
#
#        B = zeros((n1, N))
#
#        for j in range(N):
#            beq = Y1[:, j]
#            #### INCOMPLETE ###

if __name__ == '__main__':
    from scipy.io import loadmat
    data = loadmat('stable.mat')
    X = data['X']
    Y = data['Y']
    t_ols = data['ols'].squeeze()
    t_svd = data['ls_svd'].squeeze()
    t_rls_tik = data['rls_t'].squeeze()
    t_rls_tsvd = data['rls_tsvd'].squeeze()
    my_ols = OLS(X, Y, 1, 1)
    my_svd = LS_SVD(X, Y, 1, 1)
    my_rls_t = RLS_Tikhonov(X, Y, 7, 1, 1)
    my_rls_tsvd = RLS_TSVD(X, Y, 7, 1, 1)
    ols_diff = sum(t_ols - my_ols)
    svd_diff = sum(t_svd - my_svd)
    trls_diff = sum(t_rls_tik - my_rls_t)
    tsvd_diff = sum(t_rls_tsvd - my_rls_tsvd)

    print 'Diff in ols = ', ols_diff
    print 'Diff in svd = ', svd_diff
    print 'Diff in trls = ', trls_diff
    print 'Diff in trls_svd = ', tsvd_diff
